# system modules

# internal modules
import colorcorrect.colors

# external modules
import numpy as np
import statsmodels.api as sm


def reshape_image_for_regression(image):
    """
    Reshape an 3-dimensional image matrix to two dimensions with each pixel on
    as a row so it can be used for a linear regression.
    """
    return image.reshape(
        int(np.prod(image.shape) / image.shape[-1]), image.shape[-1]
    )


def prepare_image_for_regression(image):
    """
    Prepare a 3-dimensional image matrix for usage as independent variable in a
    regression. This adds a constant column after applying
    :func:`reshape_image_for_regression`
    """
    return sm.add_constant(reshape_image_for_regression(image))


def average_color(image):
    return image[:, :, : min(3, image.shape[2])].mean(
        axis=tuple(range(0, min(image.shape[2] - 1, 2)))
    )


def fit_ccm(colors, reference_colors):
    return sm.OLS(
        reshape_image_for_regression(reference_colors),
        prepare_image_for_regression(colors),
    ).fit()


def apply_ccm(image, ccm):
    return np.matmul(prepare_image_for_regression(image), ccm).reshape(
        image.shape
    )
