# system modules

# internal modules
from colorcorrect.cli import cli

# external modules


if __name__ == "__main__":
    cli(prog_name="python -m colorcorrect")
