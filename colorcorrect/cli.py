# system modules
import json
import os
import math
import sys

# internal modules
import colorcorrect
from colorcorrect.colors import (
    spyder_checkr24_colors_sRGB,
    spyder_checkr24_color_cell_regions,
)


# external modules
import numpy as np
import click
import matplotlib
import matplotlib.pyplot as plt
import statsmodels.api as sm


@click.group(
    help="Color correction matrix creation and application",
    context_settings={"help_option_names": ["-h", "--help"]},
)
def cli():
    pass


@cli.command(help="Calculate a color correction matrix")
@click.option(
    "-i",
    "--input",
    default=sys.stdin,
    help="The picture taken of the color reference chart",
    type=click.File("rb"),
)
@click.option(
    "-o",
    "--output",
    default=sys.stdout,
    help="Where the color correction matrix should be saved",
    type=click.File("w"),
)
@click.option(
    "--scale",
    help="Scaling factor for the color cell regions",
    type=click.FloatRange(min=0),
    show_default=True,
    default=0.9,
)
@click.option(
    "--show",
    is_flag=True,
    help="Show the regions and average colors",
)
def calibrate(input, output, scale, show):

    image = plt.imread(input)

    image = image[:, :, 0 : min(3, image.shape[2])]

    average_colors = []
    if show:
        fig, (ax1, ax2, ax3) = plt.subplots(ncols=3)
        ax1.set_title("Scanned Checkr 24\nwith cell regions")
        ax1.matshow(image)
    for color_cell_region in spyder_checkr24_color_cell_regions(
        height=image.shape[0], width=image.shape[1], scale=scale
    ):
        color_cell = image[color_cell_region]
        average_colors.append(colorcorrect.average_color(color_cell))
        if show:
            # show rectangle
            v, h = color_cell_region
            ax1.add_patch(
                matplotlib.patches.Rectangle(
                    (h.start, v.start),
                    h.stop - h.start,
                    v.stop - v.start,
                    facecolor="none",
                    edgecolor="blue",
                )
            )

    checkr_scan_colors = np.array(average_colors).reshape(
        spyder_checkr24_colors_sRGB.shape
    )

    if show:
        ax2.set_title("Average colors\nin scanned cells")
        ax2.matshow(checkr_scan_colors)

        ax3.set_title("Should-be\nCheckr colors")
        ax3.matshow(spyder_checkr24_colors_sRGB)
        fig.tight_layout()
        plt.show()

    fit = colorcorrect.fit_ccm(
        colors=checkr_scan_colors,
        reference_colors=spyder_checkr24_colors_sRGB / 256,
    )

    json.dump(fit.params.tolist(), output, indent=4)


@cli.command(help="Apply a color correction matrix to an image")
@click.option(
    "-i",
    "--input",
    help="The picture to correct",
    type=click.File("rb"),
    required=True,
)
@click.option(
    "-c",
    "--ccm",
    help="The color correction matrix",
    type=click.File("r"),
    required=True,
)
@click.option(
    "-o",
    "--output",
    help="Corrected output image",
    type=click.File("wb"),
    required=True,
)
def correct(input, ccm, output):
    ccm = np.array(json.load(ccm))

    image = plt.imread(input)

    corrected = colorcorrect.apply_ccm(
        image=image[:, :, 0 : min(3, image.shape[2])] / 256, ccm=ccm
    )

    clipped = np.clip(corrected, 0, 1)

    _, fmt = os.path.splitext(output.name or "")

    plt.imsave(
        output,
        np.dstack((clipped, image[:, :, 3]))
        if image.shape[2] == 4
        else clipped,
        format=fmt.replace(".", "") or "png",
    )
