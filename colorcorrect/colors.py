# system modules
import math

# internal modules

# external modules
import numpy as np


spyder_checkr24_colors_sRGB = np.array(
    [
        [[249, 242, 238], [0, 127, 159], [222, 118, 32], [98, 187, 166]],
        [[202, 198, 195], [192, 75, 145], [58, 88, 159], [126, 125, 174]],
        [[161, 157, 154], [245, 205, 0], [195, 79, 95], [82, 106, 60]],
        [[122, 118, 116], [186, 26, 51], [83, 58, 106], [87, 120, 155]],
        [[80, 80, 78], [57, 146, 64], [157, 188, 54], [197, 145, 125]],
        [[43, 41, 43], [25, 55, 135], [238, 158, 25], [76, 60, 103]],
    ]
)
"""
Colors of the  `Spyder CHECKR24
<https://www.datacolor.com/de/fotografie-design/produkte/spydercheckr-family/#spydercheckr24>`
"""


def spyder_checkr24_color_cell_regions(width, height, scale=1):
    border_vertical = 0.06875 * width
    border_horizontal = 0.04807692307692308 * height
    grid_horizontal = 0.026223776223776224 * height
    grid_vertical = 0.0375 * width
    color_cell_height = (
        height - 2 * border_horizontal - 5 * grid_horizontal
    ) / 6
    color_cell_width = (width - 2 * border_vertical - 3 * grid_vertical) / 4
    for row in range(6):
        row_top = border_horizontal + row * (
            grid_horizontal + color_cell_height
        )
        row_bottom = row_top + color_cell_height
        row_center = np.mean([row_top, row_bottom])
        for col in range(4):
            col_left = border_vertical + col * (
                grid_vertical + color_cell_width
            )
            col_right = col_left + color_cell_width
            col_center = np.mean([col_left, col_right])
            yield slice(
                math.ceil(row_center - scale * color_cell_height / 2),
                math.floor(row_center + scale * color_cell_height / 2) + 1,
            ), slice(
                math.ceil(col_center - scale * color_cell_width / 2),
                math.floor(col_center + scale * color_cell_width / 2) + 1,
            )
